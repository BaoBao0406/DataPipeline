#! python3
# BRBP_sync.py - convert data from sql server to BRBP form

import os.path, datetime, re, glob
import numpy as np
import pandas as pd
from datetime import timedelta

import win32com.client as win32
from win32com.client import constants


# Add up the restaurant revenue for Rooms Worksheet
def event_rest_revenue(ws_Rooms, Event_tmp_rest, restaurant_info, prop_name):
    # Filter by property for restaurant_info csv file to get BR restaurant cell number
    rest_et_list = restaurant_info[restaurant_info['property'] == prop_name][['restaurant_list', 'br_cell_number']].set_index('restaurant_list')
    # Convert to dictionary format
    rest_et_list = rest_et_list['br_cell_number'].to_dict()
    # Run loop for rest_et_list to paste revenue into corresponding cell
    for rest in rest_et_list.keys():
        tmp = Event_tmp_rest[Event_tmp_rest['Function Space'].str.contains(rest)]
        if tmp.empty is False:
            ws_Rooms.Range("B" + str(rest_et_list[rest])).Value = tmp['Total F&B Revenue'].sum()


# calculate free room/upgrade Concession Type 
def concession_room_calculation(ws_cn, concession_info, concession_tmp, concession_type, groupby_list, agg_method, top_values):
    # get excel row cell number in concession.csv for input
    con_pax_excel_cell = concession_info[concession_type]['concession_excel_cell']
    
    # calculate sum/mean for Quantity and Cost, drop Property field and sort by Quantity
    concession_tmp = concession_tmp.groupby(groupby_list).agg(agg_method).reset_index()
    concession_tmp = concession_tmp.drop('Property', axis=1)
    concession_tmp = concession_tmp.sort_values(by='Quantity').head(top_values)
    
    ws_cn.Range(ws_cn.Cells(con_pax_excel_cell, 3), ws_cn.Cells(con_pax_excel_cell + concession_tmp.shape[0] - 1, 5)).Value = concession_tmp.values


# calculate free F&B/Comp Breakfast Concession Type 
def concession_fb_calculation(ws_cn, concession_info, concession_tmp, groupby_list, top_values):
    # get excel row cell number in concession.csv for input
    con_pax_excel_cell = concession_info['F&B - Other']['concession_excel_cell']
    
    # calculate sum/mean for Quantity and Cost, drop Property field and sort by Quantity
    concession_fb_tmp = concession_tmp[concession_tmp['Concession Type'].str.contains(r'F&B - Other')]
    concession_fb_tmp = concession_fb_tmp.groupby(groupby_list).agg({'Cost': 'sum', 'Quantity': 'sum'}).reset_index()
    
    concession_bbf_tmp = concession_tmp[concession_tmp['Concession Type'].str.contains(r'(Breakfast ..)')]
    concession_bbf_tmp = concession_bbf_tmp.groupby(groupby_list).agg({'Cost': 'mean', 'Quantity': 'sum'}).reset_index()
    concession_tmp = pd.concat([concession_bbf_tmp, concession_fb_tmp])
    
    concession_tmp = concession_tmp.sort_values(by='Quantity').head(top_values)
    
    ws_cn.Range(ws_cn.Cells(con_pax_excel_cell, 3), ws_cn.Cells(con_pax_excel_cell + concession_tmp.shape[0] - 1, 5)).Value = concession_tmp.values
    

# Transfer data to excel Package Meeting Worksheet
def package_meeting_info(excel, wb, Event_tmp):
    # Package Meeting Worksheet
    
    Package_meeting_tmp = Event_tmp[['Property', 'Start', 'Agreed', 'Event Classification', 'Rental Check']]
    # Filter event line by Event Classification contain 'Package Meeting'
    Package_meeting_tmp = Package_meeting_tmp[Package_meeting_tmp['Event Classification'].str.contains('Package Meeting')]
    
    # Load Package Meeting info list from csv file
    package_meeting_info = pd.read_csv(os.path.abspath(os.getcwd()) + '\\Documents\\package_meeting_info.csv', header=2)
    
    # Drop NA and duplicate values for meeting_package_name_info and rental_per_pax columns
    package_meeting_rental = package_meeting_info[['meeting_package_name_info', 'rental_per_pax']].dropna().drop_duplicates(subset=['meeting_package_name_info', 'rental_per_pax'])
    # Convert package_meeting_info into dicationary
    package_meeting_rental = package_meeting_rental.set_index('meeting_package_name_info')['rental_per_pax'].to_dict()
    
    # Classify each package event into Package Name by using Rental Check
    Package_meeting_tmp['package_name'] = np.where(Package_meeting_tmp['Rental Check'] == package_meeting_rental['Full Day'], 'Full Day', 
                                                  (np.where(Package_meeting_tmp['Rental Check'] == package_meeting_rental['Half Day with Lunch'], 'Half Day with Lunch', 
                                                  (np.where(Package_meeting_tmp['Rental Check'] == package_meeting_rental['Half Day without Lunch'], 'Half Day without Lunch', 'Tailor Made')))))
    
    # Convert package_meeting_info into dicationary
    package_meeting_info_pax = package_meeting_info.set_index(['property_info', 'meeting_package_name_info'])['pax_excel_cell'].to_dict()
    package_meeting_info_day = package_meeting_info.set_index(['property_info', 'meeting_package_name_info'])['day_excel_cell'].to_dict()
    
    # Groupby Package_meeting_tmp data for Package Meeting pax and event day
    Package_pax = Package_meeting_tmp.groupby(['Property', 'package_name'])['Agreed']
    Package_day = Package_meeting_tmp.groupby(['Property', 'package_name'])['Start']
        
    # run Macro function to open Package Meeting Worksheet
    excel.Run('MeetingPackageTab.MeetingPackage1')
        
    # Package Meeting Worksheet
    ws_pk = wb.Worksheets('Meeting Packages')
    
    # update Meeting Package pax by Property by Package type
    for (Property, package_name), group in Package_pax:
        # if (Property, package_name) in Package_pax's groupby data match (property_info, meeting_package_name_info) in package_meeting_info_pax dictionary
        if (Property, package_name) in package_meeting_info_pax:
            mp_pax_excel_cell = str(package_meeting_info_pax[(Property, package_name)])
            # Sum the groupby value according to Property and package_name by Agreed pax
            ws_pk.Range(mp_pax_excel_cell).Value = group.sum()
    # update Meeting Package event day by Property by Package type
    for (Property, package_name), group in Package_day:
        # Exclude 'Tailor Made' Package Meeting for event day
        if package_name != 'Tailor Made':
            # if (Property, package_name) in Package_day's groupby data match (property_info, meeting_package_name_info) in package_meeting_info_day dictionary
            if (Property, package_name) in package_meeting_info_day:
                mp_day_excel_cell = str(package_meeting_info_day[(Property, package_name)])
                # Count the groupby value according to Property and package_name by event day
                ws_pk.Range(mp_day_excel_cell).Value = group.count()
        else:
            continue
        
    # run Macro function to close Package Meeting Worksheet
    excel.Run('MeetingPackageTab.MeetingPackageDone')


# Transfer data to excel Concession Worksheet
def concession_info(excel, wb, concession_tmp):
    
    # Load Package concession info list from csv file
    concession_type_info = pd.read_csv(os.path.abspath(os.getcwd()) + '\\Documents\\concession.csv')
    concession_type_info = concession_type_info.set_index('concession_type').to_dict(orient='index')

    # run Macro function to open Concession Worksheet
    excel.Run('ConcessionsTab.Concessions1')
    
    # Concession Worksheet
    ws_cn = wb.Worksheets('Concession Calculation')
    
    # Filter free room Concession Type data
    concession_room = concession_tmp[concession_tmp['Concession Type'].str.contains(r'(1 per .. Comp)|(FOC rooms)')]
    if concession_room.empty is False:
        # run concession_type_calculation function
        concession_room_calculation(ws_cn, concession_type_info, concession_room, 'FOC rooms', ['Concession Type', 'Property'], {'Cost': 'mean', 'Quantity': 'sum'}, 3)
        
    # Filter Upgrade Concession Type data
    concession_upgrade = concession_tmp[concession_tmp['Concession Type'].str.contains(r'(1 per .. Upgrade)|(Upgrades)')]
    if concession_upgrade.empty is False:
        # run concession_type_calculation function
        concession_room_calculation(ws_cn, concession_type_info, concession_upgrade, 'Upgrades', ['Concession Type', 'Property'], {'Cost': 'mean', 'Quantity': 'sum'}, 5)
        
    # Filter F&B Concession Type data
    concession_fb = concession_tmp[concession_tmp['Concession Type'].str.contains(r'(F&B - Other)|(Breakfast ..)')]
    if concession_fb.empty is False:
        # run concession_type_calculation function
        concession_fb_calculation(ws_cn, concession_type_info, concession_fb, ['Concession Type'], 7)
        
    # Filter out free room/upgrade/F&B Concession Type data
    concession_rest = concession_tmp[~concession_tmp['Concession Type'].str.contains(r'(1 per .. Comp)|(FOC rooms)|(1 per .. Upgrade)|(Upgrades)|(F&B - Other)|(Breakfast ..)')]
    if concession_rest.empty is False:
        # Groupby concession_tmp data for Concession info
        concession_rest = concession_rest.groupby(['Concession Type'])['Quantity', 'Cost']
    
        # update Meeting Package pax by Concession
        for con_type, group in concession_rest:
            con_pax_excel_cell = str(concession_type_info[con_type]['concession_excel_cell'])
            # calculate the groupby Quantity using sum
            ws_cn.Range("E" + con_pax_excel_cell).Value = group['Quantity'].sum()
            
            if concession_type_info[con_type]['need_unit_cost'] == True:
                ws_cn.Range("D" + con_pax_excel_cell).Value = group['Cost'].sum()

    # run Macro function to close Concession Worksheet
    excel.Run('ConcessionsTab.ConcessionsDone')
    

# Transfer data to excel Rooms Worksheet
def rooms_info(wb, BK_tmp, Event_tmp, restaurant_info):
    # Rooms Worksheet
    ws_Rooms = wb.Worksheets('Rooms')
    
    # Rooms Worksheet Part 1
    # Post As
    ws_Rooms.Range("B2").Value = BK_tmp.iloc[0]['Name']
    # Account Name
    ws_Rooms.Range("B4").Value = BK_tmp.iloc[0]['ACName']
    # Agency Name
    ws_Rooms.Range("B5").Value = BK_tmp.iloc[0]['AGName']
    # End User Region
    ws_Rooms.Range("B6").Value = BK_tmp.iloc[0]['End_User_Region__c']
    # Regional Manager
    ws_Rooms.Range("J2").Value = BK_tmp.iloc[0]['RSO_Manager__c']
    # Booking Owner
    ws_Rooms.Range("J3").Value = BK_tmp.iloc[0]['OwnerId']
    # Booking Type
    ws_Rooms.Range("J4").Value = BK_tmp.iloc[0]['nihrm__BookingTypeName__c']
    # End User Industry
    ws_Rooms.Range("J6").Value = BK_tmp.iloc[0]['End_User_SIC__c']
    # Non-Compete Clause
    if BK_tmp.iloc[0]['End_User_SIC__c'] == 1:
        ws_Rooms.Range("J6").Value = 'Yes'
    # Commission
    if BK_tmp.iloc[0]['nihrm__CommissionPercentage__c'] != None:
        # Input in Rooms Worksheet
        ws_Rooms.Range("O6").Value = BK_tmp.iloc[0]['nihrm__CommissionPercentage__c'] / 100

    # Attrition
    if BK_tmp.iloc[0]['Percentage_of_Attrition__c'] != None:
        ws_Rooms.Range("O7").Value = BK_tmp.iloc[0]['Percentage_of_Attrition__c'] / 100
    # Booking ID
    property_id_list = {'Venetian': '2', 'Conrad': '3', 'Londoner': '4', 'Parisian': '5'}
    for d in property_id_list.keys():
        # Loop for all property in property_id_list list
        if d in BK_tmp.iloc[0]['nihrm__Property__c']:
            # Primary property by ID
            ws_Rooms.Range("O" + property_id_list[d]).Value = BK_tmp.iloc[0]['Booking_ID_Number__c']
    
    # Rooms Worksheet Part 2
    # Status
    ws_Rooms.Range("B14").Value = 'Prospect'
    # Arrival Date
    ws_Rooms.Range("B15").Value = BK_tmp.iloc[0]['ArrivalDate']
    # LOS - length of stay
    ws_Rooms.Range("B16").Value = (pd.to_datetime(BK_tmp.iloc[0]['DepartureDate']) - pd.to_datetime(BK_tmp.iloc[0]['ArrivalDate'])).days
    # Request Type
    ws_Rooms.Range("B17").Value = 'New Group'
    
    # F&B minimum (Londoner does not exist in BR yet)
    property_FB_list = {'Venetian': '28', 'Conrad': '38', 'Parisian': '46'}
    for d in property_FB_list.keys():
        # Loop for all property in property_FB_list list
        if d in BK_tmp.iloc[0]['nihrm__Property__c']:
            # F&B Minimum by property
            ws_Rooms.Range("B" + property_FB_list[d]).Value = BK_tmp.iloc[0]['nihrm__FoodBeverageMinimum__c']
    
    # Food and Beverage Part
    if Event_tmp.empty is False:        
        Event_tmp_rest = Event_tmp[['Function Space', 'Event Classification', 'Food Revenue', 'Outlet Revenue', 'Beverage Revenue']]
        Event_tmp_rest['Total F&B Revenue'] = Event_tmp_rest['Food Revenue'] + Event_tmp_rest['Outlet Revenue'] + Event_tmp_rest['Beverage Revenue']
        # Exclude Package and Breakfast classification type
        Event_tmp_rest['Event Classification'].replace(np.nan, 'Empty', inplace=True)
        Event_tmp_rest = Event_tmp_rest[~Event_tmp_rest['Event Classification'].str.contains('Package|Breakfast')]
        
        # Run function for Venetian rest revenue
        event_rest_revenue(ws_Rooms, Event_tmp_rest, restaurant_info, 'VMRH')
        # Run function for Conrad rest revenue
        event_rest_revenue(ws_Rooms, Event_tmp_rest, restaurant_info, 'CMCC')
        # Run function for Parisian rest revenue
        event_rest_revenue(ws_Rooms, Event_tmp_rest, restaurant_info, 'PARIS')


# Room and Rates part
def rooms_rates_info(wb, RoomN_tmp, start_bk, bbf_inc, room_type_list, bbf_rate=164):
    
    ws_Rooms = wb.Worksheets('Rooms')
    ws_Rates = wb.Worksheets('Daily Rates')
    
    # property full name and short name
    property_rm_dict = {'The Venetian Macao': 'Venetian', 'Conrad Macao': 'Conrad', 
                        'The Londoner Macao Hotel': 'Londoner', 'The Parisian Macao': 'Parisian'}
    
    RoomN_tmp['Room'] = RoomN_tmp['Room'].replace(np.NaN, 0, regex=True)
    # Takeout all row with 0 roomnight
    RoomN_rm_tmp = RoomN_tmp[RoomN_tmp['Room'] != 0]
    # Replace property name with property_rm_dict
    RoomN_rm_tmp['Property'] = RoomN_rm_tmp['Property'].replace(property_rm_dict)
    property_rm_list = property_rm_dict.values()
    # Filter to include properties in property_rm_dict
    RoomN_rm_tmp = RoomN_rm_tmp[RoomN_rm_tmp['Property'].isin(property_rm_list)]
    
    # Find Room Block start day
    start_rm = RoomN_rm_tmp['Pattern Date'].min()
    # Calculate Difference between Room Block day and Booking day
    date_diff = (pd.to_datetime(start_rm) - start_bk).days
    
    # Convert to dictionary and replace room type
    room_type_dict = room_type_list.set_index('FDC_room_type')['room_type'].to_dict()
    RoomN_rm_tmp['Room Type'].replace(room_type_dict, inplace=True)
    
    # breakfast price
    breakfast_rate = bbf_rate
    
    # if inlude bbf add breakfast to room rate
    if bbf_inc == 'yes':
        RoomN_rm_tmp['breakfast'] = RoomN_rm_tmp['variable'].astype(int) * breakfast_rate
        RoomN_rm_tmp['Rate'] = RoomN_rm_tmp['Rate'] + RoomN_rm_tmp['breakfast']
        RoomN_rm_tmp['Room Type'] = RoomN_rm_tmp['Room Type'] + ' + ' + RoomN_rm_tmp['variable'].astype(str) + ' BBF'
        # breakfast rate input
        property_bbf_list = {'Venetian': 4, 'Conrad': 5, 'Londoner': 6, 'Parisian': 7}
        property_bbf = RoomN_rm_tmp['Property'].unique()
        for i in property_bbf:
            ws_Rooms.Cells(57, property_bbf_list[i]).Value = breakfast_rate
        
        
    # Group by to get daily pattern for room night and rates and sum all
    RoomN_rm_tmp = RoomN_rm_tmp.groupby(['Room Block Name', 'Property', 'Room Type', 'variable', 'Pattern Date'])['Room', 'Rate'].sum()
    RoomN_rm_tmp = RoomN_rm_tmp.unstack(fill_value=0)
    
    # Room table
    room_tmp = RoomN_rm_tmp['Room']
    # Rate table
    rate_tmp = RoomN_rm_tmp['Rate']
    # Calculate Ask Rate
    revenue_by_type = (room_tmp * rate_tmp).sum(axis=1)
    room_by_type = room_tmp.sum(axis=1)
    ask_rate = revenue_by_type / room_by_type

    # Paste Room Block name and Properties
    block_property = RoomN_rm_tmp.reset_index()[['Room Block Name', 'Property']]
    ws_Rooms.Range(ws_Rooms.Cells(70, 1), ws_Rooms.Cells(70 + block_property.shape[0] - 1, 2)).Value = block_property.values
    
    # Paste Room Type and Ask rate
    ask_rate = ask_rate.reset_index()[['Room Type', 0]]
    ask_rate.columns = ['Room Type', 'Ask rate']
    # loop over ask_rate table to paste value by cell into Room Type and Ask rate
    for i in range(ask_rate.shape[0]):
        # Paste Room Type to Cell (Must use Cell to paste)
        ws_Rooms.Cells(70 + i, 3).Value = ask_rate.iloc[i]['Room Type']
        # Paste Ask rate to Cell (Must use Cell to paste)
        ws_Rooms.Cells(70 + i, 5).Value = ask_rate.iloc[i]['Ask rate']

    # Paste Room table
    ws_Rooms.Range(ws_Rooms.Cells(70, 7 + date_diff), ws_Rooms.Cells(70 + room_tmp.shape[0] - 1, 7 + date_diff + room_tmp.shape[1] - 1)).Value = room_tmp.values
    
    # Paste Rate table (set j as the row difference between first row and current row during looping)
    row_diff = 0
    # Loop over rate_tmp table to paste rates
    for i in range(rate_tmp.shape[0]):
        row_diff = i * 3
        # Paste rate
        ws_Rates.Range(ws_Rates.Cells(10 + row_diff, 9 + date_diff), ws_Rates.Cells(10 + row_diff, 9 + date_diff + rate_tmp.shape[1] - 1)).Value = rate_tmp.iloc[i]
        
    
# Transfer data to excel Meeting Space Worksheet
def meeting_space_info(excel, wb, RoomN_tmp, Event_tmp, restaurant_info, oversize_event_table):
    
    # Column index for excel input by properties
    property_et_list = {'Venetian': 8, 'Conrad': 9, 'Londoner': 9, 'Parisian': 10}
    # Peak Area and Peak day
    Events_tb_tmp = Event_tmp[['Property', 'Start', 'Agreed', 'Area', 'Event Classification']]
    Events_tb_tmp.sort_values(by='Start', inplace=True)
    Events_tb_tmp.replace(np.nan, 0, inplace=True)
    
    
    # C&E Worksheet
    ws_ce = wb.Worksheets('D. C&E')
    
    # Load EventType list from csv file
    eventtype_info = pd.read_csv(os.path.abspath(os.getcwd()) + '\\Documents\\eventtype_size.csv')
    pax_list = eventtype_info['Pax'].tolist()
    eventtype_list = eventtype_info['EventType'].dropna().tolist()
    
    # Input C&E Worksheet detail
    ws_ce.Range('B5').Value = (max(Events_tb_tmp['Start']) - min(Events_tb_tmp['Start'])).days
    
    # Event pax size
    # if event table have only one event, copy event table twice to prevent error in bin
    if Events_tb_tmp.shape[0] == 1:
        Events_tb_tmp = pd.concat([Events_tb_tmp, Events_tb_tmp])
    # Sorting value and select first 5 row
    Event_max_pax = Events_tb_tmp.sort_values(by='Agreed', ascending=False).head(5).reset_index()
    # Bin by event pax
    Event_max_pax = pd.cut(Event_max_pax['Agreed'], bins=pax_list, labels=eventtype_list)
    # Event Type and Size
    ws_ce.Range('B4').Value = Event_max_pax.iloc[0]
    
    
    # Package Meeting Worksheet
    if Event_tmp[Event_tmp['Event Classification'].str.contains('Package Meeting')].empty is False:
        package_meeting_info(excel, wb, Event_tmp)
        
        
    # Meeting Space Worksheet
    ws_Events = wb.Worksheets('Meeting Space')
    
    # Loop for all property in property_et_list list
    restaurant_list = restaurant_info['restaurant_list'].tolist()
    Events_tb_tmp = Events_tb_tmp[~Events_tb_tmp['Event Classification'].isin(restaurant_list)]
    for d in property_et_list.keys():
        # Filter event by property
        Events_loop_tmp = Events_tb_tmp[Events_tb_tmp['Property'].str.contains(d)].reset_index(drop=True)
        if Events_loop_tmp.empty is False:
            # Find the row index number for max Area
            index = Events_loop_tmp['Area'].idxmax()
            # Peak meeting date by property
            ws_Events.Cells(16, property_et_list[d]).Value = str(Events_loop_tmp.iloc[index]['Start'])
            # Peak SQM by property
            ws_Events.Cells(17, property_et_list[d]).Value = Events_loop_tmp.iloc[index]['Area']
    
    # Peak Room day
    if RoomN_tmp.empty is False:
        #Room_tb_tmp = RoomN_tmp[['Property', 'Pattern Date' 'Room']]
        Room_tb_tmp = RoomN_tmp.groupby(['Property', 'Pattern Date'])['Room'].sum().reset_index()
        # Loop for all property in property_et_list list
        for d in property_et_list.keys():
            Room_loop_tmp = Room_tb_tmp[Room_tb_tmp['Property'].str.contains(d)].reset_index(drop=True)
            if Room_loop_tmp.empty is False:
                # Find the row index number for Room
                index = Room_loop_tmp['Room'].idxmax()
                # Peak room by property
                ws_Events.Cells(18, property_et_list[d]).Value = Room_loop_tmp.iloc[index]['Room']
                
    # Event table
    Events_tb_tmp = Event_tmp[['Start', 'Start Time', 'End Time', 'Event Classification', 'Setup', 'Function Space', 'Rental Revenue', 'Agreed', 'Function Space Option']]
    # Replace NaN value for Agreed and Rental Revenue
    Events_tb_tmp['Agreed'] = Events_tb_tmp['Agreed'].replace(np.NaN, 0, regex=True)
    Events_tb_tmp['Rental Revenue'] = Events_tb_tmp['Rental Revenue'].replace(np.NaN, 0, regex=True)
    # Add string 'HKD/MOP ' to steamline the format in BR
    Events_tb_tmp['Rental Revenue'] = 'HKD/MOP ' + Events_tb_tmp['Rental Revenue'].astype(str)
    Events_tb_tmp.sort_values(by='Start', inplace=True)
    Events_tb_tmp['Start'] = Events_tb_tmp['Start'].astype(str)
    
    # Paste event table only if number of event is less than 360 (360 is number of event line cell in BR)
    if Events_tb_tmp.shape[0] < 360:
        # Transfer event table to BR
        ws_Events.Range(ws_Events.Cells(24,2), ws_Events.Cells(24 + Events_tb_tmp.shape[0] - 1, 10)).Value = Events_tb_tmp.values
    # Else send the event table as attachment in the reply notification email
    else:
        oversize_table_path = os.path.abspath(os.getcwd()) + '\\Documents\\event_table.xlsx'
        # Check if oversize event table exist
        if os.path.exists(oversize_table_path):
            # delete oversize event table
            os.remove(oversize_table_path)
        Events_tb_tmp.to_excel(oversize_table_path)
        oversize_event_table = True
    
    return oversize_event_table
    

# main function for business_review_sync
def BRBP_sync(BK_tmp, RoomN_tmp, Event_tmp, concession_tmp, bbf_inc, bbf_rate, oversize_event_table):
    
    excel = win32.DispatchEx("Excel.Application")
    
    # BRBP folder location
    BRBP_folder = 'X:\\VML\\Sales\\Business_Review\\'
    # Search for excel file contain
    BRBP_file = glob.glob(BRBP_folder + 'BR Form_Macao_*.xlsm')
    wb = excel.Workbooks.Open(BRBP_file[0], None, True)

    # Load restaurant list from csv file
    restaurant_info = pd.read_csv(os.path.abspath(os.getcwd()) + '\\Documents\\restaurant_info.csv')
    
    # Run rooms info function
    rooms_info(wb, BK_tmp, Event_tmp, restaurant_info)
    
    # Find Booking start day
    start_bk = pd.to_datetime(BK_tmp.iloc[0]['ArrivalDate'])
    # Run rooms rates info function
    if RoomN_tmp.empty is False:
        # Load room_type list from csv file
        room_type_list = pd.read_csv(os.path.abspath(os.getcwd()) + '\\Documents\\room_type.csv', header=5)
        
        rooms_rates_info(wb, RoomN_tmp, start_bk, bbf_inc, room_type_list, bbf_rate)
    
    # Run meeting space info function
    if Event_tmp.empty is False:
        oversize_event_table = meeting_space_info(excel, wb, RoomN_tmp, Event_tmp, restaurant_info, oversize_event_table)
    
    # Run concession info function
    if concession_tmp.empty is False:
        concession_info(excel, wb, concession_tmp)
    
 
    # BR filename to save
    bk_year = pd.to_datetime(BK_tmp.iloc[0]['ArrivalDate']).year
    bk_month_number = str(pd.to_datetime(BK_tmp.iloc[0]['ArrivalDate']).month)
    bk_month_name = datetime.datetime.strptime(bk_month_number, "%m")
    bk_month = bk_month_number + '-' + bk_month_name.strftime("%b")
    BRBP_save_file = BRBP_folder + str(bk_year) + '\\' + bk_month
    
    # excel filename format
    post_as_name = re.sub('[^a-zA-Z0-9 \n\.]', '', BK_tmp.iloc[0]['Name'])
    excelfile_name = BK_tmp.iloc[0]['ArrivalDate'] + '_' + post_as_name + '.xlsm'
    
    # if folder not exists create folder
    if not os.path.exists(BRBP_save_file):
        os.makedirs(BRBP_save_file)
    # if filename exists, then rename file
    if os.path.exists(BRBP_save_file + '\\' + excelfile_name):
        excelfile_name = 'Copy - ' + BK_tmp.iloc[0]['ArrivalDate'] + '_' + post_as_name + '.xlsm'
        # if copy of the filename exist, delete the copy file
        if os.path.exists(BRBP_save_file + '\\' + excelfile_name):
            os.remove(BRBP_save_file + '\\' + excelfile_name)
    
    # Save as excel in BRBP saving path
    BRBP_file_path = BRBP_save_file + '\\' + excelfile_name
    wb.SaveAs(BRBP_file_path)
    # to solve the runtime error in BR before closing
    excel.EnableEvents = False
    wb.Close(True)

    return BRBP_file_path, oversize_event_table

