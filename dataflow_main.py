#! python3
# booking_dataflow_main.py - main function for booking dataflow

import pandas as pd
import datetime, os.path, os, logging
import outlook_trigger, extract_sqlserver_data, BRBP_sync, SOC_sync


# function for run each booking data in dataflow
def bk_data_dataflow(bk_row):
    try:
        # extract sql from sqlserver main function
        BK_tmp, RoomN_tmp, Event_tmp, concession_tmp = extract_sqlserver_data.extract_sqlserver_bk_data(bk_row)
        
        # BRBP main function
        # Set default value for dataflow_file_path
        bk_row['dataflow_file_path'] = 'Not Available'
        # Boolean for event size if above 260 rows
        oversize_event_table = False
        # is this a bbf inclusive
        bbf_inc = str(bk_row['Breakfast inclusive']).lower()
        # Check if this is bbf inclusive
        if bbf_inc == 'yes':
            # bbf rate
            bbf_rate = float(bk_row['Breakfast rate'])
        else:
            bbf_rate = 0
            
        # Run BR main function
        BRBP_file_path, oversize_event_table = BRBP_sync.BRBP_sync(BK_tmp, RoomN_tmp, Event_tmp, concession_tmp, bbf_inc, bbf_rate, oversize_event_table)
        # Save path to table
        bk_row['dataflow_file_path'] = BRBP_file_path
    
        # send email to reply with BR and BP path link
        outlook_trigger.reply_notification('BRBP', bk_row, oversize_event_table)
        
    except Exception as e:
        # log the error to log file
        logging.error('\n' + '*' * 50 + '\n BRBP Error for Booking ID: ' + str(bk_row['Booking ID']), exc_info=True)
        # function to send notification for failing to run
        outlook_trigger.error_notification('BRBP', bk_row, log_file)
        pass


# function for run each soc data in dataflow
def soc_data_dataflow(bk_row):
    try:
        # extract sql from sqlserver function
        dataBK, dataRental, dataBEO, dataMTG = extract_sqlserver_data.extract_sqlserver_soc_data(bk_row)
        
        # Set default value for dataflow_file_path
        bk_row['dataflow_file_path'] = 'Not Available'
            
        # Run SOC main function
        SOC_file = SOC_sync.SOC_sync(dataBK, dataRental, dataBEO, dataMTG)
        # Save path to table
        bk_row['dataflow_file_path'] = SOC_file
    
        # send email to reply with BR and BP path link
        outlook_trigger.reply_notification('SOC', bk_row, False)
    
    except Exception as e:
        # log the error to log file
        logging.error('\n' + '*' * 50 + '\n SOC Error for Booking ID: ' + str(bk_row['Booking ID']), exc_info=True)
        # function to send notification for failing to run
        outlook_trigger.error_notification('SOC', bk_row, log_file)
        pass


# dataflow template manager
def dataflow_template(function, tmp_path):

    table = pd.read_csv(os.path.abspath(tmp_path))
    table.reindex(columns=[*table.columns.tolist(), 'dataflow_file_path'])
    number_of_booking = table.shape[0]
    
    # Run every booking in each row and pass Booking ID to extract_sqlserver_data Function
    for i in range(number_of_booking):
        # get row of Booking info and pass to sql to run data
        bk_row = table.iloc[i, :]
        
        function(bk_row)
            
    # remove tmp file
    os.remove(tmp_path)

    # Close log file
    logging.shutdown() 


# main function for booking_dataflow
if __name__ == '__main__':
    
    # logging error into file
    log_file = os.path.abspath(os.getcwd()) + '\\error_log.log'
    FORMAT = '%(asctime)s %(levelname)s: %(message)s'
    logging.basicConfig(level=logging.ERROR, filename=log_file, format=FORMAT)    
    logger = logging.getLogger(__name__)
    
    # Run outlook_trigger function
    outlook_trigger.outlook_trigger()
    
    # Run extract_sqlserver_data function if there is any data save in csv
    tmp_bk_path = os.getcwd() + '\\tmp_bk.csv'
    if os.path.exists(tmp_bk_path):
        dataflow_template(bk_data_dataflow, tmp_bk_path)
        
    
    # Run extract_sqlserver_data function if there is any data save in csv
    tmp_soc_path = os.getcwd() + '\\tmp_soc.csv'
    if os.path.exists(tmp_soc_path):
        dataflow_template(soc_data_dataflow, tmp_soc_path)
