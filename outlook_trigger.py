#! python3
# outlook_trigger.py - search for keyword in outlook email Subject, and extract table in email Body if criteria is met. 
# and export to csv for later process  

import win32com.client as win32
from win32com.client import Dispatch
import datetime, os.path, re, io, os
import pandas as pd



# Function to save the unprocess booking email to folder
def save_email(msg, msg_path):
    # replace all the special character in email Subject
    msg_filename = re.sub('[^a-zA-Z0-9 \n\.]', '', msg.Subject) + '.msg'
    # Save the email to folder
    msg_path = msg_path + msg_filename
    msg.SaveAs(msg_path)

    return msg_path

# Function to get email body information in table
def extract_email_table(msg, msg_path):
    # Using read_html to get email table, then use table[0] to convert to DataFrame format
    table_tmp = pd.read_html(msg.HTMLBody, header=0, index_col=0)[0].T
    # Convert to csv to remove the index column then convert back to DatdFrame
    table_tmp = pd.read_csv(io.StringIO(table_tmp.to_csv(index=False)), sep=",")
    # Add sender email to table for reply email send
    if msg.SenderEmailType=='EX':
        table_tmp['sender'] = msg.Sender.GetExchangeUser().PrimarySmtpAddress
    else:
        table_tmp['sender'] = msg.SenderEmailAddress
    # Add msg path for process email
    table_tmp['msg_path'] = msg_path
    
    return table_tmp

# Function to Move email to specific folder and save to csv file
def process_save_email_2_csv(MsgToMove, msg_path, tmp_file_name):
    table = pd.DataFrame()
    for msg in MsgToMove:
        # run function "save_email"
        msg_location = save_email(msg, msg_path)
        # run function "extract_email_table"
        table_tmp = extract_email_table(msg, msg_location)
        
        # Check if table is empty, if not merge old to new table
        if table.empty:
            table = table_tmp
        else:
            table = pd.concat([table, table_tmp])
    table = table.reset_index().drop(['index'], axis=1)
    # Export table to csv file for later process
    table.to_csv(os.path.abspath(os.getcwd()) + tmp_file_name)


# Loop for all email within the Date Range with the keyword
def search_all_mail(filename_list, msgs, MsgToMove, KeyWord):
    for msg in msgs:
        # replace all the special character in email Subject
        msg_subject = re.sub('[^a-zA-Z0-9 \n\.]', '', msg.Subject)
        # Search for keywords in email subject
        msgSearch = KeyWord.search((msg_subject).lower())
        if (msgSearch == None) is False:
            msg_name = msg_subject + '.msg'
            # if msg file not exist in filename_list list, append to MsgToMove
            if msg_name not in filename_list:
                MsgToMove.append(msg)
                msgSearch = 'None'
                
    return MsgToMove


# Main function in outlook_trigger
def outlook_trigger():
    # Create empty list for booking and soc
    BK_MsgToMove = []
    SOC_MsgToMove = []
    # Keyword to search for email subject
    BK_KeyWord = re.compile(r'^booking dataflow')
    SOC_KeyWord = re.compile(r'^soc dataflow')
    # Email file path
    BK_email_path = os.path.abspath(os.getcwd()) + '\\Email\\'
    SOC_email_path = os.path.abspath(os.getcwd()) + '\\soc_Email\\'
    
    
    outlook = win32.Dispatch("Outlook.Application").GetNamespace("MAPI")
    inbox = outlook.GetDefaultFolder(6)
    msgs = inbox.Items
    # Date Range from last three days
    d = (datetime.date.today() - datetime.timedelta (days=1)).strftime("%m-%d-%y")

    # Search in inbox for last three days
    msgs = msgs.Restrict("[ReceivedTime] >= '" + d +"'")
    if msgs:
        # run for Booking Dataflow email
        # Get all the previous filename save (already process emails)
        bk_filename_list = os.listdir(BK_email_path)
        BK_MsgToMove = search_all_mail(bk_filename_list, msgs, BK_MsgToMove, BK_KeyWord)
        # save email and create tmp_bk.csv file
        if BK_MsgToMove:
            process_save_email_2_csv(BK_MsgToMove, BK_email_path, '\\tmp_bk.csv')
        
        # run for SOC Dataflow email
        soc_filename_list = os.listdir(SOC_email_path)
        SOC_MsgToMove = search_all_mail(soc_filename_list, msgs, SOC_MsgToMove, SOC_KeyWord)
        # save email and create tmp_soc.csv file
        if SOC_MsgToMove:
            process_save_email_2_csv(SOC_MsgToMove, SOC_email_path, '\\tmp_soc.csv')


# Function to reply notification email with BR and BP path
def reply_notification(dataflow_name, bk_row, oversize_event_table):
    outlook = win32.Dispatch("Outlook.Application")
    mail = outlook.CreateItem(0)
    mail.To = bk_row['sender']
#    mail.CC = ';'.join(['patrick.leong@sands.com.mo', 'joyce.ieong@sands.com.mo', 'tony.ho.lucas@sands.com.mo'])
    mail.CC = ';'.join(['patrick.leong@sands.com.mo'])
    mail.Subject = 'Notification for successfully create ' + dataflow_name
    mail.GetInspector
    MessageBody = "<p>" + dataflow_name + " file path :&nbsp;<a href='" + str(bk_row['dataflow_file_path']) + "'>" + str(bk_row['dataflow_file_path']) + "</a></p>"
    index = mail.HTMLbody.find('>', mail.HTMLbody.find('<body')) 
    mail.HTMLbody = mail.HTMLbody[:index + 1] + MessageBody + mail.HTMLbody[index + 1:]
    if dataflow_name == 'BRBP':
        # event row above 260 rows will send event table as attachment in reply email as BR cannot paste more than 260 rows
        if oversize_event_table:
            mail.Attachments.Add(os.path.abspath(os.getcwd()) + '\\Documents\\event_table.xlsx')
            mail.send
            # remove tmp event table
            os.remove(os.path.abspath(os.getcwd()) + '\\Documents\\event_table.xlsx')
        else:
            mail.send
    else:
        mail.send
        

# Function to send out error notification email
def error_notification(dataflow_name, bk_row, log_file):
    outlook = win32.Dispatch("Outlook.Application")
    mail = outlook.CreateItem(0)
    mail.To = bk_row['sender']
#    mail.CC = ';'.join(['patrick.leong@sands.com.mo', 'joyce.ieong@sands.com.mo', 'tony.ho.lucas@sands.com.mo'])
    mail.CC = ';'.join(['patrick.leong@sands.com.mo'])
    mail.GetInspector
    mail.Subject = 'Error Notification for Booking ID :' + str(bk_row['Booking ID']) + ' in Booking Dataflow'
    if dataflow_name == 'BRBP':
        error_Email = os.path.abspath(os.getcwd()) + '\\Email'
        mail.Subject = 'Error Notification for Booking ID :' + str(bk_row['Booking ID']) + ' in Booking Dataflow'
        MessageBody = "<p>Noted that an error occurs in your Booking, and due to this error BRBP cannot be created.</p> <p>Please double check the below information in your Booking.&nbsp;</p> \
                       <p>- Do you enter the correct Booking ID in Booking Dataflow email?</p> <p>- Do you input roomnight and room rate in your Room Block?</p> <p>- Do you enter all necessary information in the Booking?</p> <p><br></p> <p>Systems Team,</p> \
                       <p>Access the below link for Error log for this Booking:</p> <a href='" + log_file + "'>Error Log</a></p> <p>Access the link below to delete the received Email for re-run:</p> <a href='" + error_Email + "'>Email path</a></p>"
    else:
        error_Email = os.path.abspath(os.getcwd()) + '\\soc_Email'
        mail.Subject = 'Error Notification for Booking ID :' + str(bk_row['Booking ID']) + ' in SOC Dataflow'
        MessageBody = "<p>Noted that an error occurs during generating your SOC, and due to this error SOC cannot be created.</p> <p>Please double check the below information in your Booking.&nbsp;</p> \
                       <p>- Do you enter the correct Booking ID in SOC Dataflow email?</p><p><br></p> <p>Systems Team,</p> \
                       <p>Access the below link for Error log for this SOC:</p> <a href='" + log_file + "'>Error Log</a></p> <p>Access the link below to delete the received Email for re-run:</p> <a href='" + error_Email + "'>Email path</a></p>"
    index = mail.HTMLbody.find('>', mail.HTMLbody.find('<body')) 
    mail.HTMLbody = mail.HTMLbody[:index + 1] + MessageBody + mail.HTMLbody[index + 1:]
    mail.send


# Testing purpose
#outlook_trigger()
