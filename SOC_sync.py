#! python3
# SOC_sync.py - convert and combine data to generate Summary of Charges excel file

import re, os.path
import pandas as pd
import win32com.client as win32
from win32com.client import constants



# process meeting package data
def soc_process_mkg_data(dataMTG):
    # if contain Package Meeting, break it into Package Rental and Package Audio Visual
    copied_mtg_rental = dataMTG[dataMTG['Billing Description'].str.contains(r'Package Meeting')]
    copied_mtg_rental['Billing Description'] = 'Package Rental'
    copied_mtg_rental['Service Bin'] = 'Room Rental'
    copied_mtg_av = copied_mtg_rental.copy()
    copied_mtg_av['Billing Description'] = 'Package Audio Visual'
    copied_mtg_av['Service Bin'] = 'Audio Visual'
    copied_mtg_rental['AV'] = 0
    copied_mtg_av['Rental'] = 0
    # if not contain Package Meeting
    dataMTG = dataMTG[~dataMTG['Billing Description'].str.contains(r'Package Meeting')]
    dataMTG['Service Bin'] = 'F&B'
    # combine dataMTG, copied_mtg_rental and copied_mtg_av
    dataMTG = pd.concat([dataMTG, copied_mtg_rental, copied_mtg_av])
    # merge Food, AV and Rental into Unit Price
    dataMTG['Unit Price'] = dataMTG['Food'] + dataMTG['AV'] + dataMTG['Rental']
    dataMTG = dataMTG.drop(columns=['Food', 'AV', 'Rental'])
    
    dataMTG['SOF NO.'] = '-'
    dataMTG['Total Net Charge'] = dataMTG['Unit Price'] * dataMTG['Quantity']
    dataMTG['Service Charge (10%+)'] = dataMTG['Total Net Charge'] * 0.1
    dataMTG['Total Charge'] = dataMTG['Total Net Charge'] + dataMTG['Service Charge (10%+)']
    
    return dataMTG

    
# process rental data
def soc_process_rental_data(dataRental):
    dataRental['Quantity'] = 1
    # rental data not in Hall
    dataRental_rm = dataRental[~dataRental['Function Room'].str.contains(r'Hall')]
    dataRental_rm['Billing Description'] = 'Room Rental'
    dataRental_rm['Service Bin'] = 'Room Rental'
    dataRental_rm['Total Net Charge'] = dataRental_rm['Unit Price'] * dataRental_rm['Quantity']
    dataRental_rm['Service Charge (10%+)'] = dataRental_rm['Total Net Charge'] * 0.1
    # rental data in Hall
    dataRental_hall = dataRental[dataRental['Function Room'].str.contains(r'Hall')]
    dataRental_hall['Billing Description'] = 'Hall Rental'
    dataRental_hall['Service Bin'] = 'Hall Rental'
    dataRental_hall['Service Charge (10%+)'] = 0.00
    dataRental_hall['Total Net Charge'] = dataRental_hall['Unit Price']
    # combine dataRental_rm and dataRental_hall into rental data
    dataRental = pd.concat([dataRental_rm, dataRental_hall])
    dataRental = dataRental.drop(columns=['Function Room'])
    dataRental.sort_values(by=['Event Date'], inplace=True)
    dataRental['SOF NO.'] = '-'
    dataRental['Total Charge'] = dataRental['Total Net Charge'] + dataRental['Service Charge (10%+)']
    
    return dataRental


# process BEO data
def soc_process_BEO_data(dataBEO):
    dataBEO['Billing Description'] = dataBEO['SOF NO.'].copy()
    dataBEO['Total Net Charge'] = dataBEO['Unit Price'] * dataBEO['Quantity']
    dataBEO = dataBEO[dataBEO['Total Net Charge'] > 0].reset_index(drop=True)
    
    # Load SOF prefix info list from csv file
    sof_prefix_info = pd.read_csv(os.path.abspath(os.getcwd()) + '\\Documents\\soc_sof_prefix_list.csv')
    sof_prefix_info = sof_prefix_info.set_index('sof_prefix')['service_bin'].to_dict()
    
    # store row index which is not start with sof_prefix_info dictionary key
    row_index_list = []
    # loop for dictionary in sof_prefix_info
    for i in sof_prefix_info.keys(): 
        # change columns if SOF start with
        dataBEO.loc[dataBEO['SOF NO.'].str.startswith(i, na=False), 'Service Bin'] = sof_prefix_info[i]
        dataBEO.loc[dataBEO['SOF NO.'].str.startswith(i, na=False), 'Billing Description'] = '-'
        # get row index for the selected row
        row_index = list(dataBEO.loc[dataBEO['SOF NO.'].str.startswith(i, na=False)].index)
        row_index_list.extend(row_index)
    
    # change columns if SOF does not start with
    dataBEO.loc[~dataBEO.index.isin(row_index_list), 'Billing Description'] = dataBEO.loc[~dataBEO.index.isin(row_index_list), 'SOF NO.']
    dataBEO.loc[~dataBEO.index.isin(row_index_list), 'Service Bin'] = 'F&B'
    dataBEO.loc[~dataBEO.index.isin(row_index_list), 'SOF NO.'] = '-'
            
    dataBEO = dataBEO.drop(columns=['Event Classification'])
    dataBEO['Service Charge (10%+)'] = dataBEO['Total Net Charge'] * 0.1
    dataBEO['Total Charge'] = dataBEO['Total Net Charge'] + dataBEO['Service Charge (10%+)']

    return dataBEO


# combine dataRental, dataBEO, dataMTG dataframe and sort
def soc_combine_data(dataRental, dataBEO, dataMTG):
    # combine dataBEO and dataMTG data and sort
    dataSOC = pd.concat([dataBEO, dataMTG])
    dataSOC.sort_values(by=['Event Date', 'BEO'], inplace=True)
    # combine dataSOC and dataRental and do formating
    dataSOC = pd.concat([dataRental, dataSOC])
    dataSOC = dataSOC.reindex(columns = ['Event Date', 'Property', 'BEO', 'SOF NO.', 'Service Bin', 'Billing Description', 'Unit Price', 'Quantity', 'Total Net Charge', 'Service Charge (10%+)', 'Total Charge'])
    dataSOC['Event Date'] = pd.to_datetime(dataSOC['Event Date']).dt.strftime('%m/%d/%Y')
    dataSOC['Charge Method'] = ''
    dataSOC = dataSOC.fillna(0)
    dataSOC['Total Charge'] = dataSOC['Total Charge'].round(0).astype(int)
    dataSOC['POS Chk Total'] = dataSOC['Total Charge']
    dataSOC = dataSOC.reset_index(drop=True)

    return dataSOC


# save data to excel file
def transfer_to_excel(dataBK, dataSOC, file_path, template_path):
    
    excel = win32.DispatchEx("Excel.Application")
    wb = excel.Workbooks.Open(template_path, None, True)
    ws_SOC = wb.Worksheets('Summary of Charges')
    
    # Copy Post As, Charge Method and CM into SOC
    ws_SOC.Range("B2").Value = dataBK.iloc[0]['Post As']
    ws_SOC.Range("L2").Value = dataBK.iloc[0]['Charge Method']
    ws_SOC.Range("R2").Value = dataBK.iloc[0]['CM']
    # Copy dataSOC into excel file
    ws_SOC.Range(ws_SOC.Cells(23,1), ws_SOC.Cells(23 + dataSOC.shape[0] - 1, 13)).Value = dataSOC.values
    
    ws_SOC.Cells(23 + dataSOC.shape[0] + 1, 12).Value = 'Total:'
    ws_SOC.Cells(23 + dataSOC.shape[0] + 1, 13).Value = dataSOC['POS Chk Total'].sum()
    
    # filename and file path
    soc_filename = re.sub('[^a-zA-Z0-9 \n\.]', '', dataBK.iloc[0]['Post As'])
    soc_filename = dataBK.iloc[0]['ArrivalDate'] + ' SOC - ' + soc_filename + '.xls'
    SOC_file = file_path + soc_filename 
    
    # if filename exists, then rename file
    if os.path.exists(SOC_file):
        soc_filename = 'Copy - ' + soc_filename
        SOC_file = file_path + soc_filename
        # if copy of the filename exist, delete the copy file
        if os.path.exists(SOC_file):
            os.remove(SOC_file)
            
    wb.SaveAs(SOC_file)
    wb.Close(True)
    
    return SOC_file
    

# main function for SOC_sync
def SOC_sync(dataBK, dataRental, dataBEO, dataMTG):

    # temp location
#    file_path = 'I:\\10-Sales\\+Operational Reports (5Y, Restricted)\\task_scheduler\\booking_dataflow\\Testing files\\'
    
    # SOC folder location
    file_path = 'F:\\Conventions and Exhibitions\\00 - SOC\\'
    # SOC template file
    template_path = os.path.abspath(os.getcwd()) + '\\Documents\\Summary of Charges.xls'
     
    # function for processing rental data
    dataRental = soc_process_rental_data(dataRental)
    # function for processing BEO data
    dataBEO = soc_process_BEO_data(dataBEO)
    # function for processing Meeting Package data
    dataMTG = soc_process_mkg_data(dataMTG)
    
    # function for combine data
    dataSOC = soc_combine_data(dataRental, dataBEO, dataMTG)
    # function for convert to excel file
    SOC_file = transfer_to_excel(dataBK, dataSOC, file_path, template_path)
    
    return SOC_file

##################################################################################################

### For Manual run script ##

# run main function

#SOC_file = SOC_sync(dataBK, dataRental, dataBEO, dataMTG)
        
##################################################################################################
    
